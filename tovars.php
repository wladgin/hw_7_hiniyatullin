<?php
/**
 * Created by PhpStorm.
 * User: WladGita
 * Date: 16.01.2018
 * Time: 22:07
 */

$tovars = [
    ['name' => 'Apple iPhone X 64GB Silver', 'price' => 32999],
    ['name' => 'Apple iPhone X 256GB Space Gray', 'price' => 36999],
    ['name' => 'Apple iPhone 8 Plus 64GB Gold', 'price' => 27999],
    ['name' => 'Apple iPhone 8 Plus 256GB Space Gray', 'price' => 35999],
    ['name' => 'Apple iPhone 7 Plus 128GB Silver', 'price' => 25999],
    ['name' => 'Apple iPhone 7 Plus 32GB Black', 'price' => 23999],
    ['name' => 'Apple iPhone 7 128GB Black', 'price' => 22499],
    ['name' => 'Apple iPhone 7 32GB Jet Black', 'price' => 17999],
    ['name' => 'Apple iPhone 6s 64GB Space Gray', 'price' => 13999],
    ['name' => 'Apple iPhone 6s 16GB Space Gray', 'price' => 11999]
];