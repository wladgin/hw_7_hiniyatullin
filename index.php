<?php

/*
Создать два php файла: index.php и cart_add.php
В первом выводим 10 товаров в таблице которая содержит - Название товара, Цена товара и кнопка(ссылка) купить.
Кнопка купить при помощи GET параметра передает порядковый номер товара из нумерованного массива в файл cart_add.php.
Пример пользователь нажимает на покупку 4-го товара и переходит по ссылке - homework7.com/cart_add.php?buy_item=3
Файл cart_add.php проверяет, указан ли GET параметр buy_item и если он указан, добавляет в сессию порядковый номер товара, в массив user_cart.
Пример: если пользователь купил 3 товара, то $_SESSION['user_cart’] будет содержать нумерованный массив вида [2,4,5]
После добавления, показываем сообщение - Товар успешно добавлен в корзину и добавляем ссылку - вернуться в магазин.
Далее файл index.php проверяет - есть ли в сессии купленные товары и в отдельной таблице с заголовком "Корзина» отображает купленные товары.

Дополнительное задание: Хранить не только номера товаров, но и количество. $_SESSION['user_cart’] будет ассоциативным
вида array(«номер товара»=>’количество товара'), при добавлении товара который уже есть в корзине - ’количество товара' будет увеличиваться
на единицу. В таблице корзина показываем не только названия товаров, но и их количество.
*/
session_start();
require_once 'tovars.php';

echo '<h1 align="center">' . 'Товары' . '</h1>';
$template = ['Название товара', 'Цена товара', 'Купить'];

echo '<table cellpadding="5" cellspacing="0" border="1">';
echo '<tr>';
foreach ($template as $value){
    echo "<th>".$value."</th>";
}
echo '</tr>';
foreach ($tovars as $key => $tovar){
    echo '<tr>';
    echo '<td>' . $tovar['name'] . '</td>' . '<td>' . $tovar['price'] . '</td>' . '<td><a href="cart_add.php?buy_item='.$key.'">Купить</a></td>';
    echo '</tr>';
}
echo "</table>";

echo "<br>";

echo '<h2>' . 'Корзина' . '</h2>';

echo '<table cellpadding="5" cellspacing="0" border="1">';

echo '<tr>';
echo '<td>' . 'Название товара' . '</td>' . '<td>' . 'Количество' . '</td>';
echo '</tr>';

if (is_array($_SESSION['user_cart']) && count($_SESSION['user_cart'])) {
    foreach ($_SESSION['user_cart'] as $tovarId => $quantity){
        $tovar = $tovars[$tovarId];
        echo '<tr>';
        echo '<td>' . $tovar['name'] . '</td>' . '<td>' . $quantity . '</td>';
        echo '</tr>';
    }
}

echo '</table>';
