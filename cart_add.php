<?php

require_once 'tovars.php';

session_start();

$tovarId = $_GET['buy_item'];

if (isset($tovars[$tovarId])) {
    if (!isset($_SESSION['user_cart'])) {
        $_SESSION['user_cart'] = [];
    }

    if (isset($_SESSION['user_cart'][$tovarId])) {
        $_SESSION['user_cart'][$tovarId]++;
    } else {
         $_SESSION['user_cart'][$tovarId] = 1;
    }

    echo "Товар успешно добавлен!";
    echo '<br><a href="index.php">Вернуться назад</a>';
} else {
    echo "Loooool";
}
